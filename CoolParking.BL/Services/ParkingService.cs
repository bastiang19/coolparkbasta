// TODO: implement the ParkingService class from the IParkingService interface.
//       For try to add a vehicle on full parking InvalidOperationException should be thrown.
//       For try to remove vehicle with a negative balance (debt) InvalidOperationException should be thrown.
//       Other validation rules and constructor format went from tests.
//       Other implementation details are up to you, they just have to match the interface requirements
//       and tests, for example, in ParkingServiceTests you can find the necessary constructor format and validation rules.
using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using System;
using System.Collections.Generic;

namespace CoolParking.BL.Services 
{
    public class ParkingService : IDisposable
    {
        private bool disposedValue;
        private List<Vehicle> _vehicles;

        public void TopUpVehicle(string vehicleId, decimal addBalance)
        {
            Vehicle vehicle = GetVehicleById(vehicleId);

            vehicle.Balance += addBalance;
        }
        public void AddVehicle(Vehicle vehicle)
        {
            if (_vehicles.Find(x => x.Id == vehicle.Id) != null) throw new Exception("There are already vehicle with the same Id.");

            _vehicles.Add(vehicle);
        }

        public void RemoveVehicle(string vehicleId)
        {
            _vehicles.Remove(GetVehicleById(vehicleId));
        }

        public List<Vehicle> GetVehicles()
        {
            return _vehicles.GetRange(0, _vehicles.Count);
        }

        private Vehicle GetVehicleById(string vehicleId)
        {
            Vehicle vehicle = _vehicles.Find(c => c.Id == vehicleId);

            if (vehicle != null) throw new Exception($"There is no vehicle with {vehicleId} id");

            return vehicle;
        }

        public ParkingService()
        {
            _vehicles = new List<Vehicle>();
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    // TODO: dispose managed state (managed objects)
                }

                // TODO: free unmanaged resources (unmanaged objects) and override finalizer
                // TODO: set large fields to null
                disposedValue = true;
            }
        }

        public void Dispose()
        {
            // Do not change this code. Put cleanup code in 'Dispose(bool disposing)' method
            Dispose(disposing: true);
            GC.SuppressFinalize(this);
        }
    }
}